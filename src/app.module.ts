import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { AppController } from './app.controller';
import { AppRoutingModule } from './app.routing';
import { AppService } from './app.service';
import { ControllersModule } from './controllers/controllers.module';

@Module({
  imports: [
    AppRoutingModule,
    ControllersModule,
    ConfigModule.forRoot()
  ],
  controllers: [
    AppController
  ],
  providers: [
    AppService
  ],
})
export class AppModule {}
