import { Module } from '@nestjs/common';
import { JobsService } from './controllers/api/jobs/jobs.service';
import { RedisService } from './common/redis/redis.service';
import { MessageQueueService } from './common/message-queue/message-queue.service';
import { BackupService } from './core/backup/backup.service';
import { SettingsService } from './controllers/api/settings/settings.service';
import { NotificationService } from './common/notification/notification.service';

@Module({
  exports: [
    JobsService,
    RedisService,
    MessageQueueService,
    BackupService,
    SettingsService
  ],
  providers: [
    JobsService,
    RedisService,
    MessageQueueService,
    BackupService,
    SettingsService,
    NotificationService
  ]
})
export class ServicesModule {}
