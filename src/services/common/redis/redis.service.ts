import { Injectable, Logger } from '@nestjs/common';
import * as redis from 'redis';
import * as moment from 'moment';
import * as fs from 'fs';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { CONFIG_BACKUP_PATH } from 'src/constants/system-paths.const';

@Injectable()
export class RedisService {

  private redisClient: redis.RedisClient;
  private logger = new Logger(RedisService.name);
  private readonly configBackupPath = process.env.CONFIG_BACKUP_PATH || CONFIG_BACKUP_PATH;

  constructor() {
    this.redisClient = redis.createClient({
      url: process.env.REDIS_URL
    });

    if (process.env.PRODUCTION) {
      this.redisClient.on("error", (error) => {
        this.logger.error(error);
      })
    }
  }

  /**
   * Insert Record
   * 
   * @param id 
   * @param payload 
   */
  set(id, payload) {
    return new Promise((res, rej) => {
      if (!payload.created_at) {
        payload.created_at = moment().toISOString();
      }
      payload.updated_at = moment().toISOString();
      this.redisClient.set(id, JSON.stringify({
        value: payload,
      }), (error) => {
        if (error) return rej(error);
        /** Backup config into a JSON file */
        try {
          let backupString: string = "{}";
          if (fs.existsSync(this.configBackupPath)) {
            backupString = fs.readFileSync(this.configBackupPath, {encoding: 'utf8'});
            fs.unlinkSync(this.configBackupPath);
          }
          const backup = JSON.parse(backupString);
          backup[id] = payload;
          fs.appendFileSync(this.configBackupPath, JSON.stringify(backup, null, 4), {encoding: 'utf8'});
        } catch (error) {
          this.logger.error(error, 'Unable to backup the config');
        }
        return res(payload);
      })
    })
  }

  /**
   * Get Record
   * 
   * @param key 
   */
  get(key): Promise<any> {
    return new Promise((res, rej) => {
      this.redisClient.get(key, (error, reply) => {
        if (error) return rej(error);
        if (!reply) return res(null);
        try {
          return res(JSON.parse(reply).value)
        } catch (error) {
          return rej(error);
        }
      })
    })
  }

  /**
   * Scan Promise
   */
  scanPromise(
    key: string, 
    cursor: number = 0, 
    previousKeys: string[] = []
  ): Promise<[number, string[]]> {
    return new Promise((res, rej) => {
      this.redisClient.scan(cursor?.toString() || "0", "MATCH", key, (err, reply) => {
        if (err) return rej(err);
        previousKeys = previousKeys.concat(reply[1]);
        return res([Number(reply[0]), previousKeys]);
      })
    })
  }

  /**
   * MGET Promise
   * @param keys 
   */
  mgetPromise(keys: string[]): Promise<any[]> {
    return new Promise((res, rej) => {
      this.redisClient.mget(...keys, (err, values) => {
        if (err) return rej(err);
        const results = values.map(value => {
          try {
            return JSON.parse(value).value
          } catch (error) {
            return null;
          }
        });
        return res(results)
      })
    })
  }

  /**
   * Scan Keys
   */
  async scan(key: string, pagination?: PaginationDto): Promise<{results: any[], count: number}> {
    let [cursor, keys] = await this.scanPromise(key);
    while (cursor) {
      const [newCursor, newKeys] = await this.scanPromise(key, cursor, keys);
      keys   = newKeys;
      cursor = newCursor;
    }

    if (!keys.length) return {
      results: [],
      count: 0
    }

    const results = await this.mgetPromise(
      keys.slice(
        (Number(pagination.index || 0) * Number(pagination.size || 0)), 
        (Number((pagination.index || 0) + 1) * Number(pagination.size || keys.length))
      )
    );

    return {
      results,
      count: keys.length
    }
  }

}
