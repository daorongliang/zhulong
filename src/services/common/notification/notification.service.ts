import { HttpStatus, Injectable, Logger } from '@nestjs/common';
import * as nodemailer from 'nodemailer';
import * as path from 'path';
import * as pug from 'pug';
import * as moment from 'moment';
import SMTPTransport from 'nodemailer/lib/smtp-transport';
import { RedisService } from '../redis/redis.service';
import { SystemSettings } from 'src/enums/system-settings.enum';
import { SMTPConfigInterface } from 'src/interfaces/stmp-config.interface';

@Injectable()
export class NotificationService {

  private logger = new Logger(NotificationService.name);

  constructor(
    private _redis: RedisService
  ) {

  }

  /**
   * Send Email
   * 
   * @param options 
   */
  async sendMail(options: {
    html          : string,
    from         ?: string,
    recipients    : string[] | string,
    subject       : string,
    attachments  ?: any[],
    sendTogether ?: boolean,
    withTemplate ?: boolean
  }) {
    /** Generate Send Content */
    let contentToSend: string = options.html; 
    if (options.withTemplate) {
      contentToSend = pug.renderFile(
        path.join(__dirname, '../../../pugs/email.template.pug'),
        {
          moment,
          content: options.html
        }
      )
    }

    /** Format Recipients */
    if (typeof options.recipients == 'string') {
      options.recipients = options.recipients.split(",");
    }

    /** Initial Mail Server Connection */
    let transporter: nodemailer.Transporter<SMTPTransport.SentMessageInfo> ;
    try {
      /** Get SMTP Config */
      const smtpConfig: SMTPConfigInterface = await this._redis.get(SystemSettings.SMTP_SETTINGS);
      if (!smtpConfig) throw {
        message: 'SMTP config is missing.',
        statusCode: HttpStatus.NOT_FOUND
      }

      /** Connect */
      transporter = nodemailer.createTransport({
        host: smtpConfig.smtp_host,
        port: Number(smtpConfig.smtp_port || 587),
        secure: Number(smtpConfig.smtp_port || 587) == 465,
        auth: {
          user: smtpConfig.smtp_username,
          pass: smtpConfig.smtp_password
        }
      });

      /** Initial send result */
      let info: SMTPTransport.SentMessageInfo | SMTPTransport.SentMessageInfo[];
      if (!options.from) {
        options.from = "Scheduled Backup 🥟";
      }
      /** Do not send out one by one */
      if (options.sendTogether) {
        info = await transporter.sendMail({
          from        : `"${options.from}" <${smtpConfig.smtp_username}>`,
          to          : options.recipients.join(","),
          subject     : options.subject,
          html        : contentToSend,
          attachments : options.attachments || []
        })
      }
      /** Send out one by one */
      else {
        info = await Promise.all(
          options.recipients.map(email => {
            return transporter.sendMail({
              from        : `"${options.from}" <${smtpConfig.smtp_username}>`,
              to          : email,
              subject     : options.subject,
              html        : contentToSend,
              attachments : options.attachments || []
            })
          })
        )
      }
      return info;
    } catch (error) {
      this.logger.error(error);
    } finally {
      /** Close Mail Server Connection */
      if (transporter?.close)
        transporter.close();
    }
  }
}
