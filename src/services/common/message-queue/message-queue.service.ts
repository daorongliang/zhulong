import { Injectable } from '@nestjs/common';
import { pandaMQ } from 'pdmq';
import { PDMQClient } from 'pdmq/dist/client';
import { PDMQConsumer } from 'pdmq/dist/consumer';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { JobStatus } from 'src/enums/job-status.enum';
import { JobInterface } from 'src/interfaces/job.interface';
import { BackupService } from 'src/services/core/backup/backup.service';
import { RedisService } from '../redis/redis.service';

@Injectable()
export class MessageQueueService {
  public messageQueueClient: PDMQClient;
  private messageQueueConsumer: PDMQConsumer;

  constructor(
    private _backup: BackupService,
    private _redis: RedisService
  ) {
    this.initialMessageQueue();
    
  }

  async initialMessageQueue() {
    const pdmq = new pandaMQ({
      redis_url: process.env.REDIS_URL,
      debug: process.env.PRODUCTION ? false : true
    });
  
    const {
      client,
      consumer,
    } = await pdmq.init();
    
    this.messageQueueConsumer = consumer;
    this.messageQueueClient   = client;

    consumer.taskQueue.subscribe(async (task: PDMQTask) => {
      /** Load Job Detail */
      const job: JobInterface = await this._redis.get(`jobs:${task.task_id}`);

      /** Verify Job Exists and Active */
      if (!job) return;
      else if (job.job_status != JobStatus.ACTIVE) return;

      /** Run JOb */
      this._backup.backup({
        job,
        task
      });
    })
  }
}
