import { Injectable, Logger } from '@nestjs/common';
import { PDMQTask } from 'pdmq/dist/interfaces/task.interface';
import { JobInterface } from 'src/interfaces/job.interface';
import execSh from 'exec-sh'
import * as moment from 'moment';
import * as path from 'path';
import * as fs from 'fs-extra';
import * as os from 'os';
import * as archiver from 'archiver';
import { NotificationService } from 'src/services/common/notification/notification.service';

@Injectable()
export class BackupService {

  private logger = new Logger(BackupService.name);

  constructor(
    private _notification: NotificationService
  ) {}

  /**
   * Run backup job
   * 
   * @param options 
   */
  async backup(options: {
    job : JobInterface,
    task: PDMQTask
  }) {
    let error: any, replacedBackups: string[] = [];
    try {
      /** Verify Source Exists */
      const sourceExist = fs.existsSync(options.job.job_source);
      if (!sourceExist)
        throw `Source does not exist: ${options.job.job_source}`;

      /** Verify Destination Exists */
      const destinationExist = fs.existsSync(options.job.job_destination);
      if (!destinationExist)
        throw `Destination does not exist: ${options.job.job_destination}`;

      /** Verify Source is a folder or file */
      const sourceIsDirectory = fs.lstatSync(options.job.job_source).isDirectory();
      const sourceIsFile      = fs.lstatSync(options.job.job_source).isFile();
      
      if (!sourceIsDirectory && !sourceIsFile)
        throw "Unsupported source type"

      /** Get Destination Path */
      const destinationStat = fs.readdirSync(options.job.job_destination);
      
      const copyCount = destinationStat.filter(fileName => {
        const timestamp = fileName.split(".")[0];
        return timestamp && moment(timestamp, 'x', true).isValid();
      }).sort((a, b) => b > a ? -1 : 1);

      /** Archive Folder / File */
      if (options.job.job_archive_folder) {
        await this.archivePromise(options.job, {
          sourceIsDirectory
        });
      }
      /** Archive not required, run normal copy */
      else {
        const destination = options.job.job_destination + '/' + moment().format('x') + '.' + options.job.job_name;
        fs.mkdirSync(destination);
        fs.copySync(options.job.job_source, destination);
      }
      
      this.logger.verbose(`Job ${options.job.job_name} Completed`)


      /** Remove Extra Copies */
      if (copyCount.length >= (options.job.job_number_of_copy || 3)) {
        const replaceBackupNames = copyCount.slice(0, copyCount.length - (options.job.job_number_of_copy || 3) + 1);
        
        replaceBackupNames.forEach(backupName => {
          const backupPath        = options.job.job_destination + '/' + backupName;
          const backupIsDirectory = fs.lstatSync(backupPath).isDirectory();
          const backupIsFile      = fs.lstatSync(backupPath).isFile();
          if (backupIsFile)
            fs.unlinkSync(backupPath);
          if (backupIsDirectory)
            this.deleteFolderRecursive(backupPath);
          
          replacedBackups.push(backupName);
        })
      }
    } 
    /** Error Handling */
    catch (err) {
      this.logger.error(`${options.job.job_name}, ${moment().format("DD MMM YYYY HH:mm")}, ${err}`);
      error = err
    }
    /**
     * Log Job
     */
    finally {
      try {
        /** Write Log to File */
        const logFilePath = options.job.job_destination + '/' + 'zhulong-log.html';
        fs.appendFileSync(logFilePath, `
          <div>
            <h4 style="margin: 0;">
              Job: ${options.job.job_name}
            </h4>
            <h5 style="margin: 0;">
              Backup Result: ${error ? 'Failed' : 'Succeed'}
            </h5>
            <small>Completed at: ${moment().format("DD MMM YYYY HH:mm")}</small>
            <table style="margin-top: 10px;">
              <tbody>
                <tr>
                  <th style="text-align: left">
                    Source
                  </th>
                  <td>
                    ${options.job.job_source}
                  </td>
                </tr>
                <tr>
                  <th style="text-align: left">
                    Destination
                  </th>
                  <td>
                    ${options.job.job_destination}
                  </td>
                </tr>
                ${
                  error ? `
                    <tr>
                      <th style="text-align: left">
                        Error Detail
                      </th>
                      <td>
                        ${error.toString()}
                      </td>
                    </tr>
                  ` : ''
                }
                ${
                  replacedBackups?.length ? replacedBackups.map((item, i) => {
                    return `<tr>
                      <th style="text-align: left">
                        Replaced Backup ${i + 1}
                      </th>
                      <td>
                        ${item}
                      </td>
                    </tr>`
                  }).join("")
                  : ''
                }
              </tbody>
            </table>
          </div>
          <hr />
        `)

        /** Send Email Notification */
        if (options.job.job_recipients)
          await this._notification.sendMail({
            html: `
            <p>Job ${options.job.job_name}: ${error ? '<b style="color: red;">FAILED</b>' : '<b style="color: green;">SUCCEED</b>'}</p>
            ${
              error && `
                <p>
                  Error Detail: ${error}
                </p>
              ` || ""
            }
            `,
            recipients: options.job.job_recipients,
            subject: `Job ${options.job.job_name} ${error ? 'FAILED' : 'SUCCEED'}`,
            withTemplate: true
          })
      } catch (error) {
        this.logger.error(`Unable to log job: ${error}`);
      }
    }
  }


  /**
   * Archive Promise
   */
  archivePromise(job: JobInterface, options: {
    sourceIsDirectory: boolean
  }) {
    return new Promise((res, rej) => {
      /** Initialise Archiver */
      const outputStream = fs.createWriteStream(job.job_destination + `/${moment().format("x")}.${job.job_name}.zip`);
      const archive = archiver('zip');

      outputStream.on('close', () => {
        this.logger.verbose("Zip Completed");
        res(1);
      });

      archive.on('warning', (err) => {
        this.logger.warn(err);
      });

      archive.on('error', function(err) {
        rej(err);
      });
      archive.pipe(outputStream);

      /** Append Content */
      if (options.sourceIsDirectory) {
        archive.directory(job.job_source, false)
      } else {
        archive.file(job.job_source, {name: job.job_source.split('/')[job.job_source.split('/').length - 1]})
      }

      /** Start Archive */
      archive.finalize();
    })
  }

  /**
   * Delete Non Empty Folder Util
   * @param path 
   */
  deleteFolderRecursive(path) {
    if( fs.existsSync(path) ) {
      fs.readdirSync(path).forEach((file) => {
        var curPath = path + "/" + file;
        if(fs.lstatSync(curPath).isDirectory()) { // recurse
          this.deleteFolderRecursive(curPath);
        } else { // delete file
          fs.unlinkSync(curPath);
        }
      });
      fs.rmdirSync(path);
    }
  };
}
