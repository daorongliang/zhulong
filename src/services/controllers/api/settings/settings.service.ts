import { HttpStatus, Injectable } from '@nestjs/common';
import { SmtpDto } from 'src/dto/settings/smtp.dto';
import { SystemSettings } from 'src/enums/system-settings.enum';
import { RedisService } from 'src/services/common/redis/redis.service';
import * as fs from 'fs';
import path from 'path';
import { CONFIG_BACKUP_PATH } from 'src/constants/system-paths.const';

@Injectable()
export class SettingsService {

  constructor(
    private _redis: RedisService
  ) {}

  /**
   * Save SMTP Settings
   * 
   * @param payload 
   */
  async saveSmtpSettings(payload: SmtpDto) {
    if (!payload.smtp_host || !payload.smtp_password || !payload.smtp_port || !payload.smtp_username)
      throw {
        statusCode: HttpStatus.BAD_REQUEST,
        message: "Invalid input, missing fields"
      }

    await this._redis.set(SystemSettings.SMTP_SETTINGS, payload);
  }

  /**
   * Get SMTP Settings
   */
  getSmtpSettings() {
    return this._redis.get(SystemSettings.SMTP_SETTINGS);
  }

  /**
   * Export Config Backup
   */
  exportConfigBackup() {
    return fs.createReadStream(process.env.CONFIG_BACKUP_PATH || CONFIG_BACKUP_PATH);
  }

}
