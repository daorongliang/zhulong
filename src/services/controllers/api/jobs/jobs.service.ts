import { Injectable, HttpStatus } from '@nestjs/common';
import { JobCreateDto } from 'src/dto/entities/job.create.dto';
import { RedisService } from 'src/services/common/redis/redis.service';
import { cloneDeep } from 'lodash';
import { v1 } from 'uuid';
import { MessageQueueService } from 'src/services/common/message-queue/message-queue.service';
import { JobUpdateDto } from 'src/dto/entities/job.update.dto';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { JobInterface } from 'src/interfaces/job.interface';
import * as path from 'path';
import * as fs from 'fs';

@Injectable()
export class JobsService {

  constructor(
    private _redis: RedisService,
    private _messageQueue: MessageQueueService
  ) {}

  /**
   * Create Job
   */
  async create(payload: JobCreateDto) {
    const newJob = cloneDeep(payload);
    
    const task = await this._messageQueue.messageQueueClient.addTask({
      task_name                   : payload.job_name,
      task_trigger_type           : payload.job_trigger_type,
      task_trigger_once_datetime  : payload.job_trigger_once_datetime,
      task_trigger_time           : payload.job_trigger_time,
      task_trigger_days           : payload.job_trigger_days,
      task_trigger_date           : payload.job_trigger_date,
      task_trigger_months         : payload.job_trigger_months,
      task_trigger_years          : payload.job_trigger_years,
      task_fallback_retry_limit   : payload.job_fallback_retry_limit,
      task_fallback_retry_duration: payload.job_fallback_retry_duration,
      task_fallback_task_id       : payload.job_fallback_job_id,
    })

    newJob.job_id = task.task_id;
    await this._redis.set(`jobs:${task.task_id}`, newJob);
  }

    /**
   * Update Job
   */
    async update(jobId: string, payload: JobUpdateDto) {
      const exitedJob = await this._redis.get(`jobs:${jobId}`);
      if (!exitedJob) {
        throw {
          statusCode: HttpStatus.NOT_FOUND,
          message: "Job not found"
        }
      }
      const newPayload = {
        ...cloneDeep(exitedJob),
        ...payload
      };

      await this._messageQueue.messageQueueClient.updateTask({
        task_id                     : jobId,
        task_name                   : newPayload.job_name,
        task_trigger_type           : newPayload.job_trigger_type,
        task_trigger_once_datetime  : newPayload.job_trigger_once_datetime,
        task_trigger_time           : newPayload.job_trigger_time,
        task_trigger_days           : newPayload.job_trigger_days,
        task_trigger_date           : newPayload.job_trigger_date,
        task_trigger_months         : newPayload.job_trigger_months,
        task_trigger_years          : newPayload.job_trigger_years,
        task_fallback_retry_limit   : newPayload.job_fallback_retry_limit,
        task_fallback_retry_duration: newPayload.job_fallback_retry_duration,
        task_fallback_task_id       : newPayload.job_fallback_job_id,
      })
      await this._redis.set(`jobs:${jobId}`, newPayload);
    }

    /**
     * List all jobs
     * 
     * @param pagination 
     */
    listAll(pagination: PaginationDto) {
      // await this._redis.listAll()
      return this._redis.scan("jobs:*", pagination);
    }

    /**
     * return logs in html
     * 
     * @param jobId 
     */
    async fetchLogs(jobId: string): Promise<string> {
      const job = await this.getJob(jobId);
      const logPath = path.join(job.job_destination, 'zhulong-log.html');
      if (fs.existsSync(logPath)) {
        return fs.readFileSync(logPath, {encoding: 'utf8'});
      } else {
        return "No Log Entry Found"
      }
    }

    /**
     * get job by id
     * 
     * @param jobId 
     */
    getJob(jobId): Promise<JobInterface> {
      return this._redis.get(`jobs:${jobId}`);
    }

}
