import { Body, Controller, Get, Logger, Param, Post, Query, Render, Req, Res } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { JobUpdateDto } from 'src/dto/entities/job.update.dto';
import { JobsService } from 'src/services/controllers/api/jobs/jobs.service';

@Controller('jobs')
export class JobsController {

  private logger = new Logger(JobsController.name);

  constructor(
    private _jobs: JobsService
  ) {}

  @ApiExcludeEndpoint()
  @Get()
  @Render('jobs')
  async index(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: PaginationDto
  ) {
    try {
      const {results, count} = await this._jobs.listAll(query);
      return {
        jobs: results,
        noJobFound: !count,
        count
      }
    } catch (error) {
      return {
        error
      }
    }
  }

  @ApiExcludeEndpoint()
  @Get('/add')
  @Render('jobs-edit')
  async addJob(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      return {
        isCreating: true
      }
    } catch (error) {
      return {
        error
      }
    }
  }


  @ApiExcludeEndpoint()
  @Get('/:jobId/edit')
  @Render('jobs-edit')
  async editJob(
    @Req() req: Request,
    @Res() res: Response,
    @Param() params: {jobId: string}
  ) {
    try {
      /** Load Job Detail */
      const job = await this._jobs.getJob(params.jobId);

      /** Render */
      return {
        isCreating: false,
        ...job
      }
    } catch (error) {
      this.logger.error(error, `[GET /:jobId/edit]`);
      return {
        error
      }
    }
  }
  

  @ApiExcludeEndpoint()
  @Post('/save')
  @Render('jobs-saved')
  async saveJob(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: JobUpdateDto,
    @Query() query: {job_id?: string}
  ) {
    try {
      if (query.job_id) {
        await this._jobs.update(query.job_id, body);
      }
      else {
        await this._jobs.create(body);
      }

      return {
        success: true,
        isCreating: !!query.job_id
      }
    } catch (error) {
      this.logger.error(error, `[POST /save]`);
      return {
        success: false,
        error,
      }
    }
  }

}
