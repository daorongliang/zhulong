import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { IndexController } from './index/index.controller';
import { JobsController } from './jobs/jobs.controller';
import { SettingsController } from './settings/settings.controller';
import { AboutController } from './about/about.controller';

@Module({
  imports: [
    ServicesModule
  ],
  controllers: [IndexController, JobsController, SettingsController, AboutController]
})
export class PagesModule {}
