import { Controller, Get, Render } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';

@Controller('about')
export class AboutController {


  @ApiExcludeEndpoint()
  @Get()
  @Render('about')
  about(
  ) {
    return {}
  }

}
