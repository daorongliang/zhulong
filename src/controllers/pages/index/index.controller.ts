import { Controller, Get, Render, Req, Res } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
import { Request, Response } from 'express';

@Controller()
export class IndexController {

  @ApiExcludeEndpoint()
  @Get()
  index(
    @Req() req: Request,
    @Res() res: Response
  ) {
    return res.redirect('/jobs')
  }

}
