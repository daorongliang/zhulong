import { Controller, Get, Render, Req, Res } from '@nestjs/common';
import { ApiExcludeEndpoint } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { SettingsService } from 'src/services/controllers/api/settings/settings.service';

@Controller('settings')
export class SettingsController {

  constructor(
    private _settings: SettingsService
  ) { }

  @ApiExcludeEndpoint()
  @Get()
  @Render('settings')
  async settings(
    @Req() req: Request,
    @Res() res: Response,
  ) {
    try {
      const smtp = await this._settings.getSmtpSettings();
      return {
        ...smtp
      } 
    } catch (error) {
      res.redirect(`/settings?error=${error.message || JSON.stringify(error)}`)
    }
  }

}
