import { Module } from '@nestjs/common';
import { ServicesModule } from 'src/services/services.module';
import { JobsController } from './jobs/jobs.controller';
import { SettingsController } from './settings/settings.controller';

@Module({
  imports: [
    ServicesModule
  ],
  controllers: [
    JobsController,
    SettingsController
  ]
})
export class ApiModule {}
