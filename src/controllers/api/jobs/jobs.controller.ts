import { Body, Controller, Get, HttpStatus, Logger, Param, Patch, Post, Put, Query, Req, Res } from '@nestjs/common';
import { ApiParam, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { PaginationDto } from 'src/dto/common/pagination.dto';
import { JobCreateDto } from 'src/dto/entities/job.create.dto';
import { JobUpdateDto } from 'src/dto/entities/job.update.dto';
import { JobsService } from 'src/services/controllers/api/jobs/jobs.service';

@ApiTags("Jobs")
@Controller('jobs')
export class JobsController {

  private logger = new Logger(JobsController.name);
  
  constructor(
    private _jobs: JobsService
  ) {}

  @Get()
  async listAll(
    @Req() req: Request,
    @Res() res: Response,
    @Query() query: PaginationDto
  ) {
    try {
      const {results, count} = await this._jobs.listAll(query);
      res.status(HttpStatus.OK).send({
        statusCode: HttpStatus.OK,
        message: {
          results, 
          count
        }
      })
    } catch (error) {
      res.status(error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).send({
        statusCode: error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR,
        message: error.message || error
      })
    }
  }


  @Put()
  async create(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: JobCreateDto
  ) {
    try {
      await this._jobs.create(body);
      res.status(HttpStatus.OK).send({
        statusCode: HttpStatus.OK
      })
    } catch (error) {
      this.logger.error(error);
      res.status(error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).send({
        statusCode: error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR,
        message: error.message || error
      })
    }
  }

  @ApiParam({
    name: "jobId",
    type: String
  })
  @Get(':jobId/logs')
  async viewJobLogs(
    @Req()   req: Request,
    @Res()   res: Response,
    @Param() params: { jobId: string }
  ) {
    try {
      const logHtml = await this._jobs.fetchLogs(params.jobId);
      res.status(HttpStatus.OK).send(logHtml);
    } catch (error) {
      this.logger.error(error);
      res.status(error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).send({
        statusCode: error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR,
        message: error.message || error
      })
    }
  }

  @ApiParam({
    name: "jobId",
    type: String
  })
  @Patch(':jobId')
  async update(
    @Req()   req: Request,
    @Res()   res: Response,
    @Param() params: { jobId: string },
    @Body()  body: JobUpdateDto
  ) {
    try {
      await this._jobs.update(params.jobId, body);
      res.status(HttpStatus.OK).send({
        statusCode: HttpStatus.OK
      })
    } catch (error) {
      this.logger.error(error);
      res.status(error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR).send({
        statusCode: error.statusCode || HttpStatus.INTERNAL_SERVER_ERROR,
        message: error.message || error
      })
    }
  }
}
