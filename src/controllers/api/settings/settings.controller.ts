import { Body, Controller, Get, HttpStatus, Logger, Post, Req, Res } from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { Request, Response } from 'express';
import { SmtpDto } from 'src/dto/settings/smtp.dto';
import { SettingsService } from 'src/services/controllers/api/settings/settings.service';

@ApiTags("Settings")
@Controller('settings')
export class SettingsController {

  private logger = new Logger(SettingsController.name);

  constructor(
    private _settings: SettingsService
  ) {}
  
  @ApiOperation({
    description: "Set up SMTP for system email notification"
  })
  @Post('/smtp')
  async saveSmtpSettings(
    @Req() req: Request,
    @Res() res: Response,
    @Body() body: SmtpDto
  ) {
    try {
      await this._settings.saveSmtpSettings(body);
      res.redirect('/settings?saved=1');
    } catch (error) {
      this.logger.error(error);
      res.redirect('/settings?error=' + error.message || JSON.stringify(error));
    }
  }

  @ApiOperation({
    description: "Export application configs for backup purpose"
  })
  @Post('/export-configs')
  async exportConfigBackup(
    @Req() req: Request,
    @Res() res: Response
  ) {
    try {
      const readStream = this._settings.exportConfigBackup();
      res.download(readStream.path as string)
    } catch (error) {
      this.logger.error(error);
      res.redirect('/settings?error=' + error.message || JSON.stringify(error));
    }
  }

}
