import { Module } from '@nestjs/common';
import { ApiModule } from './api/api.module';
import { PagesModule } from './pages/pages.module';

@Module({
  imports: [
    ApiModule,
    PagesModule
  ]
})
export class ControllersModule {}
