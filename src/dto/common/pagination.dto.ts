import { ApiProperty } from '@nestjs/swagger';

export class PaginationDto {
  @ApiProperty({
    required: false,
    default: 0
  })
  index: number;

  @ApiProperty({
    required: false,
    default: 1
  })
  size: number;

  @ApiProperty({
    required: false,
    default: 'updated_at'
  })
  sortBy: string;

  @ApiProperty({
    required: false,
    default: 'desc'
  })
  direction: 'asc' | 'desc';
}