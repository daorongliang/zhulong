import { ApiProperty } from '@nestjs/swagger';

export class JobUpdateDto {
  @ApiProperty()
  job_name: string;

  @ApiProperty()
  job_source: string;

  @ApiProperty()
  job_destination: string;

  @ApiProperty()
  job_number_of_copy: number;

  @ApiProperty()
  job_trigger_type: 'once' | 'daily' | 'weekly' | 'monthly' | 'annually' | 'never' | 'instant';

  /** ISO date time string */
  @ApiProperty()
  job_trigger_once_datetime?: string;

  /** eg. '00:00:00' | '10:10:30' | '19:30:30' | '23:59:59' */
  @ApiProperty()
  job_trigger_time?: string;

  /** eg. [1, 2, 3, 4, 5, 6, 7] */
  @ApiProperty()
  job_trigger_days?: number[];

  /** eg. 01 | 15 | 28 */
  @ApiProperty()
  job_trigger_date?: number;

  /** eg. ['01', '12'] */
  @ApiProperty()
  job_trigger_months?: string[];

  /** eg. [2021, /20[0-9]2/] */
  @ApiProperty()
  job_trigger_years?: (string | number)[];

  @ApiProperty()
  job_fallback_retry_limit?: number;

  /** In Seconds */
  @ApiProperty()
  job_fallback_retry_duration?: number;

  /** In Seconds */
  @ApiProperty()
  job_fallback_job_id?: string;
}