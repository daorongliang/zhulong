import { ApiProperty } from "@nestjs/swagger";

export class SmtpDto {
  @ApiProperty()
  smtp_host: string;

  @ApiProperty()
  smtp_username: string;

  @ApiProperty()
  smtp_password: string;

  @ApiProperty()
  smtp_port: number;
}