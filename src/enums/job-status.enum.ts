export enum JobStatus {
  RUNNING  = 'running',
  ACTIVE   = 'active',
  DISABLED = 'disabled'
}