export interface SMTPConfigInterface {
  smtp_host     : string,
  smtp_username : string,
  smtp_password : string,
  smtp_port     : string
}