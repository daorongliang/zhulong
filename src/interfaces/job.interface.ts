import { JobStatus } from "src/enums/job-status.enum";

export interface JobInterface {

  job_id: string;


  job_name: string;


  job_source: string;


  job_destination: string;


  job_number_of_copy: number;


  job_archive_folder?: boolean;


  job_status: JobStatus;

  job_recipients?: string[];


  job_trigger_type: 'once' | 'daily' | 'weekly' | 'monthly' | 'annually' | 'never' | 'instant';

  /** ISO date time string */

  job_trigger_once_datetime?: string;

  /** eg. '00:00:00' | '10:10:30' | '19:30:30' | '23:59:59' */

  job_trigger_time?: string;

  /** eg. [1, 2, 3, 4, 5, 6, 7] */

  job_trigger_days?: number[];

  /** eg. 01 | 15 | 28 */

  job_trigger_date?: number;

  /** eg. ['01', '12'] */

  job_trigger_months?: string[];

  /** eg. [2021, /20[0-9]2/] */

  job_trigger_years?: (string | number)[];


  job_fallback_retry_limit?: number;

  /** In Seconds */

  job_fallback_retry_duration?: number;

  /** In Seconds */

  job_fallback_job_id?: string;
}
