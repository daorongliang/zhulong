import { Module } from '@nestjs/common';
import { ApiModule } from './controllers/api/api.module';
import { RouterModule, Routes } from "nest-router";

const routes: Routes = [
  {
    path: '/api',
    module: ApiModule,
  },
];

@Module({
imports: [
    RouterModule.forRoutes(routes), // setup the routes
    ApiModule
], // as usual, nothing new
})
export class AppRoutingModule {}