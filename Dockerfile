FROM node:14.17.0-slim

COPY ./ /application

RUN npm i --prefix /application

RUN npm i -g pm2

RUN npm run build --prefix /application

EXPOSE 3000

ENTRYPOINT [ "pm2-runtime", "/application/dist/main.js" ]